button = {}
buttonbg = love.graphics.newImage("images/ui/buttonLong_beige.png")
buttonbg_active = love.graphics.newImage("images/ui/buttonLong_brown.png")
buttonbg_x = (buttonbg:getWidth( ) / 2) - 2
buttonbg_y = (buttonbg:getHeight( ) / 2) - 2
function button_spawn(x, y, text)
	table.insert(button, {x = x, y = y, text = text, mouseover = false})
end

function button_draw()
	for i, v in ipairs(button) do
		if v.mouseover == true then
			love.graphics.draw(buttonbg_active, v.x, v.y)
			
		else
			love.graphics.draw(buttonbg, v.x, v.y)
			
		end
		love.graphics.print( v.text, v.x + buttonbg_x - menuf:getWidth(v.text) / 2, v.y + buttonbg_y - menuf:getHeight() / 2)
		--love.graphics.setColor(255, 255, 255)
	end
end
function button_click(x, y)
	for i, v in ipairs(button) do
		if mousex > v.x and mousex < v.x + buttonbg_x * 2 and mousey > v.y and mousey < v.y + buttonbg_y * 2 then
			-- tady co bude delat cudlik cele prepsat-
			if gamestate == menu_text then
				gamestate = v.text
			end
			if gamestate == "game" then
				if v.text == return_text then
					action = nil
					chat = ""
				else
					if v.text == fight_text then
						--udp:send("boj "..prezdivka.." "..nepritel)
					elseif v.text == domesta_text then
						udp:send("jdido "..kam)
					elseif v.text == heal_text then
						udp:send("heal "..prezdivka)
					else
						action = v.text
					end
					
				end
			end
		end
	end
end
function button_check()
	for i, v in ipairs(button) do
		if mousex > v.x and mousex < v.x + buttonbg_x * 2 and mousey > v.y and mousey < v.y + buttonbg_y * 2 then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end
function button_clear()
	for k in pairs (button) do
		button [k] = nil
	end
end
