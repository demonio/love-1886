-- nacteni postavicek
postavicka = {}
svet = {}
boj_vojaci = {}
boj_hraci = {}
penize = ""
lvl = ""
obr = ""
kam = nil
koho = nil
done = false
action = nil
narod = nil
selected = ""
chat = ""
chat_input = ""
mesto = ""
tick = 0
ATL = require("AdvTiledLoader")
tx, ty = 0, 0 
ATL.Loader.path = 'maps/'
map = ATL.Loader.load("01.tmx") 
tileLayer = map.layers["spodek"]
for i=0,25,1 do 
	postavicka[i] = love.graphics.newImage("images/postavicka/".. i + 1 ..".png")
end
game_bg = love.graphics.newImage("images/map/bg.png")
game_bg:setWrap("repeat", "repeat")
srdce = love.graphics.newImage("images/hud/srdce.png")
orb = love.graphics.newImage("images/hud/mana.png") 
inventar = love.graphics.newImage("images/hud/inventar.png")
menu = love.graphics.newImage("images/ui/menu.png")
action_bg = love.graphics.newImage("images/hud/action_bg.png")
fight_bg = love.graphics.newImage("images/bg/fight.jpg")
-- nastavení sítě --
local socket = require "socket"
local address, port = "37.205.11.163", 12345
local updaterate = 0.1 -- how long to wait, in seconds, before requesting an update
-- %s string %d cislo
-- nastaveni kurzoru --
cursor = love.mouse.newCursor("images/ui/cursorSword_silver.png", 0, 0)
cursor_active = love.mouse.newCursor("images/ui/cursorGauntlet_blue.png", 0, 0)
love.mouse.setCursor( cursor )
-- casem predelat aby se i menil --
	bg800 = love.graphics.newImage("images/bg/800.jpg")
	bg1024 = love.graphics.newImage("images/bg/1024.jpg")
	bg1366 = love.graphics.newImage("images/bg/1366.jpg")
	bg1600 = love.graphics.newImage("images/bg/1600.jpg")
	bg1920 = love.graphics.newImage("images/bg/1920.jpg")
	bg3000 = love.graphics.newImage("images/bg/3000.jpg")

inputbg = love.graphics.newImage("images/ui/input.png")
-- nacteni obrazku vseobecnych
love.graphics.setBackgroundColor( 63, 123, 182 )

-- nacteni komponentu -- 
menuf = love.graphics.newFont("fonts/active.ttf", 25)
require "menu-button"
require "active-menu"
require "language/CZ"
require "game"
require "prompt"
require "settings"
require "boj-menu"
gamestate = menu_text
prezdivka = ""
log = ""
odpoved = ""
vystup = ""
local co, ceho, phase
function love.load()
   min_dt = 1/60
   next_time = love.timer.getTime()
    udp = socket.udp()
    udp:settimeout(0)
    udp:setpeername(address, port)

	if gamestate == menu_text then
		love.graphics.setFont( menuf )
	end
	
end
function love.update(dt)
	if phase == 2 and gamestate == "game" then
		if tick == 10 then
			if svet[prezdivka].x then
				udp:send("update "..prezdivka.." "..svet[prezdivka].x.." "..svet[prezdivka].y.." "..svet[prezdivka].mapa)
			end
			tick = 0
		else
			tick = tick + 1
		end
	end
	next_time = next_time + min_dt
	if gamestate == "game" and phase == 2 then
		mousex = love.mouse.getX() - tx
		mousey = love.mouse.getY() - ty
	else
		mousex = love.mouse.getX()
		mousey = love.mouse.getY()
		tx = 0
		ty = 0
	end
    	width = love.graphics.getWidth( )
    	height = love.graphics.getHeight( )
	data, msg = udp:receive()
	if data then
		log = data
		print(data)
		if data == "mocdaleko" then
			vystup = faraway_text
		end
		if string.find(data, "aktualizace") then
				a, meno, meno_x, meno_y, char, zivoty, mapa = data:match("^(.*) (.*) (.*) (.*) (.*) (.*) (.*)")
				if meno == prezdivka then
					mapa = mapa
					map = ATL.Loader.load(mapa) 
					tileLayer = map.layers["spodek"]
					if svet[meno] == nil then
						--print("funguje")
						char = love.graphics.newImage("images/postavicka/".. char + 1 ..".png")
						svet[meno] = {x=meno_x, y=meno_y, character=char, zivoty=zivoty, mapa = mapa}
						-- opravit
						tx, ty = -meno_x+(width/2-char:getWidth()/2), -meno_y+(height/2-char:getHeight()/2)
					else
						svet[meno].zivoty = zivoty
					end
				else
					char = love.graphics.newImage("images/postavicka/".. char + 1 ..".png")
					svet[meno] = {x=meno_x, y=meno_y, character=char, zivoty=zivoty, mapa = mapa}
				end
	 	end
		co, ceho = data:match("^(%S*) (.*)")
		if co == "phase" then
			ceho = ceho + 1
			ceho = ceho - 1
			phase = ceho
			--print(phase)
		end
        if co == "teleport" then
            teleport(ceho)
            vystup = kam
            mesto = kam
	    svet[prezdivka].x = 704
	    svet[prezdivka].y = 704
        end
		if co == "send_chat" then
			chat = chat.."\n \n"..ceho 
		end
		if co == "inventar" then
			--print(ceho)
			penize, lvl = ceho:match("^(.*) (.*)")
			
		end
		if co == "jdi_do" then
			narod, kam = ceho:match("^(.*) (.*)")
			
		end
		if co == "protihrac_akce" then
			koho = ceho
		end
		if data == "nahravani_hotovo" then
			done = true
		end
	end
	if gamestate == register_text then
		if log == "Uzivatel vytvoren" then
			odpoved = register_odpoved_true
		elseif log == "Uzivatelske jmeno je jiz zabrano" then
			odpoved = register_odpoved_false
		end
	end
	if gamestate == login_text then
		if log == "Uzivatel neexzistuje" then
			odpoved = login_odpoved_false
		elseif log == "change_gamestate" then
			gamestate = "game"
		end
	end
	if gamestate == menu_text then
		mousex = love.mouse.getX()
		mousey = love.mouse.getY()
		button_clear()
		button_spawn(100, 100, register_text)
		button_spawn(100, 200, login_text)
		button_spawn(100, 300, options_text)
		button_spawn(100, 400, quit_text)
		button_check()
	end
	if gamestate == quit_text then
		love.event.quit()
	end
	if gamestate == "game" then
		game_update(dt, phase)
		button_check()
	end
end
function love.textinput(t)
    if gamestate == register_text then
	    prezdivka = prezdivka .. t
    end
    if gamestate == login_text then
            prezdivka = prezdivka .. t
    end
    if gamestate == "game" then
		game_textinput(t, phase)
	end
    if action == "chat" then
        chat_input = chat_input .. t
    end
end
function love.draw()
	--love.graphics.setColor(0, 0, 0)
	--love.graphics.setColor(255, 255, 255)
	if gamestate == "game" then
		quad = love.graphics.newQuad(0, 0, width, height, 64, 64)
		love.graphics.draw(game_bg, quad, 0,0)
	else
		if width < 801 then
			love.graphics.draw(bg800, 0, 0)
		elseif width < 1025 then
			love.graphics.draw(bg1024, 0, 0)
		elseif width < 1367 then
			love.graphics.draw(bg1366, 0, 0)
		elseif width < 1601 then
			love.graphics.draw(bg1600, 0, 0)
		elseif width < 1921 then
			love.graphics.draw(bg1920, 0, 0)
		else
			love.graphics.draw(bg3000, 0, 0)
		end
	end
	if gamestate == options_text then
		settings()
	end
   -- Display the frame time in milliseconds for convenience.
   -- A lower frame time means more frames per second.
       --love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
	if gamestate == menu_text then
		button_draw()
		love.graphics.setColor(0, 0, 0)
		love.graphics.printf(menu_new_text, (width / 2), 100, love.graphics.getWidth() / 3)
		love.graphics.setColor(255, 255, 255)
	end
    if gamestate == register_text then
	love.graphics.setColor(0, 0, 0)
        love.graphics.printf(register_new_text, (width / 2) - (menuf:getWidth(register_new_text) / 2), (height / 2) - 100, love.graphics.getWidth())
	love.graphics.setColor(255, 255, 255)
        love.graphics.draw(inputbg,(width / 2) - (inputbg:getWidth() / 2), (height / 2) - (inputbg:getHeight() / 3) )
        love.graphics.printf(prezdivka, (width / 2) - (menuf:getWidth(prezdivka) / 2), (height / 2), love.graphics.getWidth())
	love.graphics.printf(odpoved, (width / 2) - (menuf:getWidth(odpoved) / 2), (height / 2)+ 50, love.graphics.getWidth())
	odpoved = ""
    end
    if gamestate == login_text then
	love.graphics.setColor(0, 0, 0)
        love.graphics.printf(register_new_text, (width / 2) - (menuf:getWidth(register_new_text) / 2), (height / 2) - 100, love.graphics.getWidth())
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(inputbg,(width / 2) - (inputbg:getWidth() / 2), (height / 2) - (inputbg:getHeight() / 3) )
        love.graphics.printf(prezdivka, (width / 2) - (menuf:getWidth(prezdivka) / 2), (height / 2), love.graphics.getWidth())
	love.graphics.printf(odpoved, (width / 2) - (menuf:getWidth(odpoved) / 2), (height / 2)+ 50, love.graphics.getWidth())
    end
	if gamestate == "game" then
		game_draw(phase)
	end
	love.graphics.print(vystup, (width/2) - menuf:getWidth(log)/2 - tx, 10 - ty)
		love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10 - tx, 10 - ty)
   if action == "chat" then
		love.graphics.printf(chat, width/2 + 50 - action_bg:getWidth()/2 - tx , height/2 + 25 - action_bg:getHeight()/2 - ty, 500)
        love.graphics.printf(chat_input, width/2 + 25 - action_bg:getWidth()/2 - tx , height/2 - 100 + action_bg:getHeight()/2 - ty, 400)
   end
   local cur_time = love.timer.getTime()
   if next_time <= cur_time then
      next_time = cur_time
      return
   end
   love.timer.sleep(next_time - cur_time)
end
function love.mousepressed(x, y, button)
	if button == "l" then
		if gamestate == menu_text then
			button_click(x, y)
		end
	end
	if gamestate == "game" then
		game_mousepressed(x,y,button, phase)
		button_click(x, y)
	end
end
function love.keypressed(key)
    if phase == 2 then
    	map:callback("keypressed", key)
    end
    if key == "escape" then
        if gamestate == menu_text then
            love.event.quit()
        else
            gamestate = menu_text
        end
    end
    if key == "backspace" then
        if gamestate == register_text then
            prezdivka = ""
        end
        if gamestate == login_text then
                prezdivka = ""
        end
        if action == "chat" then
               chat_input = ""
        end
    end
    if key == "return" then
	    if gamestate == register_text then
                  udp:send("register_prezdivka"..prezdivka)
	    end
	    if gamestate == login_text then
		    udp:send("login_prezdivka"..prezdivka)
	    end
        if action == "chat" then
            udp:send("chat_message "..prezdivka..": "..chat_input)
            chat_input = ""
            chat = ""
        end
    end
    if action == "chat" then
    else
	    if gamestate == "game" then
		    game_keypressed(key, phase)
	    end
    end
end
