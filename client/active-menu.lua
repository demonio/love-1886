active = {}
activebg = love.graphics.newImage("images/ui/buttonLong_beige.png")
activebg_active = love.graphics.newImage("images/ui/buttonLong_brown.png")
activebg_x = (activebg:getWidth( ) / 2) - 2
activebg_y = (activebg:getHeight( ) / 2) - 2
function active_spawn(x, y, text, number, phase)
	table.insert(active, {x = x, y = y, text = text, mouseover = false, number = number, phase=phase})
end

function active_draw(phase)
	for i, v in ipairs(active) do
		if v.mouseover == true then
			love.graphics.setColor(0,0, 255)
			
		else
			love.graphics.setColor(255, 255, 255)
			
		end
		if phase == 2 then
			if tonumber(svet[v.number].zivoty) <= 0 and tonumber(svet[v.number].zivoty) > -100 then
				love.graphics.setColor(0,0, 0)
			end
		end
		love.graphics.draw( v.text, v.x, v.y)
		love.graphics.setColor(255, 255, 255)
	end
end
function active_click(x, y, button)
	for i, v in ipairs(active) do
		if mousex > v.x and mousex < v.x + v.text:getWidth() and mousey > v.y and mousey < v.y + v.text:getHeight() then
			-- tady co bude delat cudlik cele prepsat-
			--print(v.phase)
			if button == "l" then
				if v.phase == 1 then
					udp:send(string.format("%s %d %s", "character", v.number, prezdivka))
				end
				if v.phase == 2 then
					--print(v.number)
					udp:send(string.format("%s %s %s", "action", v.number, prezdivka))
				end
			else
				udp:send(string.format("%s %s %s", "boj", v.number, prezdivka))
			end
		end
	end
end
function active_check()
	for i, v in ipairs(active) do
		if mousex > v.x and mousex < v.x + v.text:getWidth() and mousey > v.y and mousey < v.y + v.text:getHeight() then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end
function active_clear()
	for k in pairs (active) do
		active [k] = nil
	end
end
