function teleport(map)
	mapa = map
        svet[prezdivka].mapa = map
	map = ATL.Loader.load(map) 
	tileLayer = map.layers["spodek"]
	mesto = ""	
	for k in pairs (svet) do
		if k == prezdivka then
		else
			svet[k] = nil
		end
	end
	if string.find(mapa, "0") then
		udp:send("reload_mesta")
	else
		udp:send("reload_mapa_data "..kam)
	end
end

function player_move(x,y)
	local tileX, tileY = svet[prezdivka].x/64, svet[prezdivka].y/64
	local tile = tileLayer(tileX + x, tileY + y)
	if tile == nil then return end
	if tile.properties.obstacle then return end
	if tile.properties.teleport then
		teleport(tile.properties.teleport)
		return
	end
	kam = nil
	koho = nil
	narod = nil 
	vystup = ""
	svet[prezdivka].y = svet[prezdivka].y + y * 64
	svet[prezdivka].x = svet[prezdivka].x + x * 64
	ty = -svet[prezdivka].y+(height/2-svet[prezdivka].character:getHeight()/2)
	tx = -svet[prezdivka].x+(width/2-svet[prezdivka].character:getWidth()/2)
end
function game_update(dt, phase)
	if phase == 1 then
		active_clear()
		for i=0,7,1 do 
			active_spawn((width / 2) - (4 * 64) + (64 * (i)), height - 75*3, postavicka[i] , i, phase)
		end
		for i=8,15,1 do 
			active_spawn((width / 2) - (4 * 64) + (64 * (i - 8)), height - 75*2, postavicka[i] , i, phase)
		end
		for i=16,23,1 do 
			active_spawn((width / 2) - (4 * 64) + (64 * (i - 16)), height - 75, postavicka[i] , i, phase)
		end
		active_check()
	end
	if phase == 2 then
		if love.keyboard.isDown("up") then ty = ty + 250*dt end
    		if love.keyboard.isDown("down") then ty = ty - 250*dt end
    		if love.keyboard.isDown("left") then tx = tx + 250*dt end
    		if love.keyboard.isDown("right") then tx = tx - 250*dt end
		active_clear()
		button_clear()
		button_spawn(width - 195 - tx, 150 - ty, camp_text)
		for i, v in pairs(svet) do
            if v.mapa == svet[prezdivka].mapa then
			    active_spawn(tonumber(v.x), tonumber(v.y), v.character, i, phase)
            end
		end
		if action then
			prompt(action)
		else
			if kam then
				if narod == "Bandits" then
					button_spawn(width/2 - 100 -tx, height/2 -50 -ty, domesta_text)
					button_spawn(width/2 - 100 - tx, height/2 - ty, verbovat_text)
				else
					button_spawn(width/2 - 100 - tx, height/2 -100 -ty, heal_text)
					button_spawn(width/2 - 100 - tx, height/2 -50 -ty, verbovat_text)
					button_spawn(width/2 - 100 - tx, height/2 - ty, nakupovat_text)
					button_spawn(width/2 - 100 -tx, height/2 +50 -ty, domesta_text)
					button_spawn(width/2 - 100 - tx, height/2 +100 - ty, obsadit_text)
				end
			end
			if koho then
				--button_spawn(width/2 - 100 - ty, height/2 -50 - ty, bojovat_text)
				button_spawn(width/2 - 100 - ty, height/2 - ty, nakupovat_text)
				--button_spawn(width/2 - 100, height/2 +50, mluvit_text)
			end
		end
		active_check()
	end
end
function game_draw(phase)
	if phase == 1 then
		love.graphics.setColor(0, 0, 0)
        	love.graphics.printf(prolog_text, 10, 10, love.graphics.getWidth())
		love.graphics.printf(newcharacter_text, (width / 2) - (menuf:getWidth(newcharacter_text) / 2), height / 2, love.graphics.getWidth())
		love.graphics.setColor(255, 255, 255)
		active_draw(phase)
	end
	if phase == 2 then
    			love.graphics.translate( math.floor(tx), math.floor(ty) )
    			map:autoDrawRange( math.floor(tx), math.floor(ty), 1, pad)
 			map:draw()
			active_draw(phase)
            for i, v in pairs(svet) do
                if v.mapa == svet[prezdivka].mapa then
		            love.graphics.print(i, v.x - (menuf:getWidth(i) / 2)+v.character:getWidth()/2,v.y-25)
		            for i=1,v.zivoty,1 do
			            love.graphics.draw(srdce, (v.x + v.character:getWidth()/2) - v.zivoty/2 * srdce:getWidth() + ((i-1)*srdce:getWidth()) , v.y - 40)
		            end
                end
	        end
			if penize then
				love.graphics.draw(inventar, width - inventar:getWidth() - tx , 10-ty)
				local penize_vystup = penize_text.." "..penize
				love.graphics.setColor(0, 0, 0)
				love.graphics.printf(penize_vystup,width - inventar:getWidth()/2- menuf:getWidth(penize_vystup)/2 - tx, 20 - ty, inventar:getWidth() - 20)
				lvl_vystup = lvl_text.." "..lvl
				love.graphics.printf(lvl_vystup,width - inventar:getWidth()/2- menuf:getWidth(lvl_vystup)/2 - tx , 75 - ty, inventar:getWidth())
				love.graphics.setColor(255, 255, 255)
			end
			if action then
				prompt_draw(action)
				fight_draw()
			end
			button_draw()
	end
end
function game_keypressed(k, phase)
    if phase == 2 then
        if k == 'w' then player_move(0,-1) end
        if k == 'a' then player_move(-1,0) end
        if k == 's' then player_move(0,1) end
        if k == 'd' then player_move(1,0) end
	if k == 'c' then
		action = "chat"
		prompt("chat")
		udp:send("open_chat")
	end
    end
end
function game_mousepressed(x, y, button, phase)
	if button == "l" then
		active_click(x, y, button)
		if action then
			fight_click(x, y)
		end
	end
	if button == "r" then
		active_click(x, y, button)
	end
end
function game_textinput(t, phase)
end

