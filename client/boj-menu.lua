fight = {}
fightbg = love.graphics.newImage("images/ui/buttonLong_beige.png")
fightbg_fight = love.graphics.newImage("images/ui/buttonLong_brown.png")
fightbg_x = (fightbg:getWidth( ) / 2) - 2
fightbg_y = (fightbg:getHeight( ) / 2) - 2
function fight_spawn(x, y, text, hrac)
	table.insert(fight, {x = x, y = y, text = text, mouseover = false, hrac=hrac, vybran = false})
end

function fight_draw()
	for i, v in ipairs(fight) do
		if selected == v.hrac then
			love.graphics.setColor(255,0, 0)
		else
			if v.mouseover == true then
				love.graphics.setColor(0,0, 255)
			
			else
				love.graphics.setColor(255, 255, 255)
			
			end
		end
		love.graphics.draw( v.text, v.x, v.y)
		love.graphics.setColor(255, 255, 255)
	end
end
function fight_click(x, y)
	for i, v in ipairs(fight) do
		if mousex > v.x and mousex < v.x + v.text:getWidth() and mousey > v.y and mousey < v.y + v.text:getHeight() then
			-- tady co bude delat cudlik cele prepsat-
			--print(v.phase)
			--local string = string.format(" fight_step %s %s %s", v.hrac, prezdivka, selected) 
			--print(string)
			if selected == v.hrac then
				selected = ""
			else
				selected = v.hrac
			end
		end
	end
end
function fight_check()
	for i, v in ipairs(fight) do
		if mousex > v.x and mousex < v.x + v.text:getWidth() and mousey > v.y and mousey < v.y + v.text:getHeight() then
			v.mouseover = true
			--print(v.vybran)
		else
			v.mouseover = false
		end
	end
end
function fight_clear()
	for k in pairs (fight) do
		fight [k] = nil
	end
end
