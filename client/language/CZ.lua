login_text = "Přihlašení"
register_text = "Registrace"
options_text = "Nastavení"
quit_text = "Konec"
menu_text = "Menu"
register_new_text = "Zadejte vaší přezdívku"
menu_new_text = "Vítejte v naší nové hře"
prolog_text = "Kontinentem Archenar od smrti krále Julia II. vládnou nepokoje. Páni kteří dříve přislíbily věrnost koruně, se nyní odvrací a prohlašují se za nové krále a dědice bohatství země Lysithea. Kontinent ohrožuje ale větší hrozba než občanská válka. Barbarské kmeny žijící vysoko v horách vycítily šanci, využít válku pro svou pomstu. Slézají z hor a vyvražďují vesnice a města, které rytíři nedokážou kvůli své touze po bohatství chránit. Ve snaze zachránit rozpolcený kontinent se Královská rada upíná k poslední naději a tou jsi ty."
register_odpoved_true = "A nyni se muzete prihlasit"
register_odpoved_false = "Zvolte jinou prezdivku tato je jiz zabrana"
login_odpoved_false = "Prihlaseni neprobehlo uspesne"
newcharacter_text = "Vyberte si jak bude vaše postavička vypadat"
penize_text = "Peníze"
lvl_text = "Úroveň"
camp_text = "Utábořit"
faraway_text = "Příliš daleko"
verbovat_text = "Verbovat"
obsadit_text = "Obsadit"
nakupovat_text = "Obchodovat"
bojovat_text = "Napadnout"
return_text = "Zpátky"
breakcamp_text = "Utábořit se"
zalozitosadu_text = "Nová osada"
jednotky_text = "Armáda"
skills_text = "Hrdina"
fight_text = "Bojovat"
autofight_text = "Auto boj"
run_text = "Utéct"
attack_text = "Útok"
block_text = "Bránit"
select_text = "Nejdříve vyberte na koho chcete útočit"
heal_text = "Uzdravit"
domesta_text = "Vstoupit"
