#!/usr/bin/python

# -----------------------------------------------------------------
# imports

import sys, os 
import socket
import mysql.connector
import threading
    
# -----------------------------------------------------------------
# init

reload(sys)
sys.setdefaultencoding('utf-8')
cnx = mysql.connector.connect(user='root', password='smookynda', host='127.0.0.1', database='love-1886')
cursor = cnx.cursor()
prezdivka = ""
svet = []
UDP_PORT = 12345
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind(('', UDP_PORT))



# -----------------------------------------------------------------
# classes


class myThread (threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        print "Starting " + self.name
        player()    # TODO  - params
        print "Exiting " + self.name



# -----------------------------------------------------------------
# register new player


def register(data, addr):
    prezdivka = data.replace("register_prezdivka","",1)
    check_user = 'select count(1) from user where prezdivka="%s"' % prezdivka
    
    print(check_user)
    
    cursor.execute(check_user)
    results = cursor.fetchone() # TODO - check number of results
    found = results[0]
    if found == 0:
        add_user = 'INSERT INTO user (prezdivka) VALUES ("%s")' % prezdivka
        cursor.execute(add_user)
        cnx.commit()
        add_user = 'INSERT INTO armada (prezdivka) VALUES ("%s")' % prezdivka
        cursor.execute(add_user)
        cnx.commit()
        sock.sendto("Uzivatel vytvoren", (addr))
    else:
        sock.sendto("Uzivatelske jmeno je jiz zabrano", (addr))


# -----------------------------------------------------------------
# player login


def login(data, addr):
    prezdivka = data.replace("login_prezdivka","",1)
    check_user = 'select count(1) from user where prezdivka="%s"' % prezdivka
    cursor.execute(check_user)
    results = cursor.fetchone()
    found = results[0]
    if found > 0:
        sock.sendto("change_gamestate", (addr))
        phase_user = 'SELECT phase, prezdivka, x, y, obrazek, zivoty, mapa  From user Where prezdivka="%s"' % prezdivka
        cursor.execute(phase_user)
        results = cursor.fetchone()
        found = results[0]
        sock.sendto("aktualizace %s %d %d %s %d %s" % (results[1], results[2], results[3], results[4], results[5], results[6]), (addr))
        #musi bit pred phase aby bylo jasno kde je hrac predtim nez se ma nacist
        sock.sendto("phase %d" % found, (addr))
        query = "SELECT nazev, x, y, obrazek, mapa FROM mapa"
        cursor.execute(query)
        for (nazev, x, y, obrazek, mapa) in cursor:
            sock.sendto("aktualizace %s %d %d %s %d %s" % (nazev, x, y, obrazek-1, -100, mapa), (addr))
    else:
        sock.sendto("Uzivatel neni", (addr))


# -----------------------------------------------------------------
# character select


def character(cislo, prezdivka, addr):
    update_user = 'UPDATE user SET phase="2", obrazek="%s" WHERE prezdivka="%s"' % (cislo, prezdivka)
    cursor.execute(update_user)
    cnx.commit()
    sock.sendto("phase 2", (addr))


# -----------------------------------------------------------------
# player loop

def player():
    while True:
        
        # TODO - client keepalive, if client does not respond, exit thread
        
        data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        
        if data:
            print(data)     # debug

        # TODO - player op



# -----------------------------------------------------------------
# main program loop - waiting for player login


while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    
    if data:
        print(data)     # debug
   
        #register
        if data.find("register_prezdivka") > -1:
            register(data, addr)
   
        #login
        elif data.find("login_prezdivka") > -1:
            tmp = data
            login(data, addr)
            
            #try:
            #    thread = myThread(tmp)     # name of the player
            #    thread.start()

            #except:
            #    print "Error: unable to start thread"


            # TODO - pridat postavu do mapy -> dale muzeme provadet operace s postavami v mape
            # klient by mel napriklad znat sve id hrace nebo index v nejake strukture, aby se nemusela stale odkazovat db
        
        #character select
        elif data.find("character") > -1:
            pokus = data.split()
            character(pokus[1], pokus[2], addr)        
        
        #akce s postavou
        if data.find("action") > -1:
            character = data.split()
            kdo = character[1]
            prezdivka = character[2]
            phase_user = 'SELECT x, y From user Where prezdivka="%s"' % prezdivka
            cursor.execute(phase_user)
            results = cursor.fetchone()
            x = results[0]
            y = results[1]
            check_user = 'select count(1) from user where prezdivka="%s"' % kdo
            #print(check_user)
            cursor.execute(check_user)
            results = cursor.fetchone()
            found = results[0]
            if found == 0:
                phase_user = 'SELECT x, y, narod From mapa Where nazev="%s"' % kdo
                cursor.execute(phase_user)
                results = cursor.fetchone()
                druhej_x = results[0]
                druhej_y = results[1]
                promena = "jdi_do %s" % results[2]
            else:
                phase_user = 'SELECT x, y From user Where prezdivka="%s"' % kdo
                cursor.execute(phase_user)
                results = cursor.fetchone()
                druhej_x = results[0]
                druhej_y = results[1]
                promena = "protihrac_akce"
            if (-100 < (x - druhej_x) < 100) and (-100 < (y - druhej_y) < 100):
                sock.sendto("%s %s" % (promena, kdo), (addr))
            else:
                sock.sendto("mocdaleko", (addr))

        #boj ve hre
        if data.find("boj") > -1:
            boj = data.split()
            
            user1 = 'SELECT x, y From user Where prezdivka="%s"' % boj[1]
            cursor.execute(user1)
            user1 = cursor.fetchone()
            
            user2 = 'SELECT x, y From user Where prezdivka="%s"' % boj[2]
            cursor.execute(user2)
            user2 = cursor.fetchone()

            if (abs(user1[0]) - abs(user2[0])) > 200 or (abs(user1[1]) - abs(user2[1])) > 200:
                sock.sendto("mocdaleko", (addr))
            else:
                # TODO
                boj = data.split()
                phase_user = 'SELECT zivoty, obrana From user Where prezdivka="%s"' % boj[1]
                cursor.execute(phase_user)
                results = cursor.fetchone()
                phase_user = 'SELECT sila From user Where prezdivka="%s"' % boj[2]
                cursor.execute(phase_user)
                results2 = cursor.fetchone()
                if results[0] > 0:
                     zivoty = results[0] - (results2[0] - results[1])
                     update_user = 'UPDATE user SET zivoty="%d" WHERE prezdivka="%s"' % (zivoty, boj[1])
                     cursor.execute(update_user)
                     cnx.commit()   
        
        if data.find("heal") > -1:
            boj = data.split()
            update_user = 'UPDATE user SET zivoty="%d" WHERE prezdivka="%s"' % (10, boj[1])
            cursor.execute(update_user)
            cnx.commit()   
            query = 'SELECT prezdivka, x, y, obrazek, zivoty, mapa FROM user Where prezdivka = "%s"' % boj[1]
            cursor.execute(query)
            results = cursor.fetchone()
            sock.sendto("aktualizace %s %d %d %s %d %s" % (results[0], results[1], results[2], results[3], results[4], results[5]), (addr))
            
        #update postav
        if data.find("update") > -1:
            boj = data.split()
            update_user = 'UPDATE user SET x="%s", y="%s", mapa="%s" WHERE prezdivka="%s"' % (boj[2], boj[3], boj[4], boj[1])
            cursor.execute(update_user)
            cnx.commit()
            query = 'SELECT prezdivka, x, y, obrazek, zivoty, mapa FROM user'
            cursor.execute(query)
            for (prezdivka, x, y, obrazek, zivoty, mapa) in cursor:
                sock.sendto("aktualizace %s %d %d %s %d %s" % (prezdivka, x, y, obrazek, zivoty, mapa), (addr))
                #print(prezdivka, x, y, obrazek)
            phase_user = 'SELECT penize, lvl From user Where prezdivka="%s"' % boj[1]
            cursor.execute(phase_user)
            results = cursor.fetchone()
            sock.sendto("inventar %d %d" % (results[0], results[1]), (addr))
        data = None


# -----------------------------------------------------------------


